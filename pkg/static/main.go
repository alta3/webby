package static

import (
	"net/http"

	"github.com/gorilla/mux"
)

func ConfigureStaticFiles(r *mux.Router) *mux.Router {
	staticFileDir := http.Dir("web/static/")
	staticFileHandler := http.StripPrefix("/static/", http.FileServer(staticFileDir))
	r.PathPrefix("/static/").Handler(staticFileHandler).Methods("GET")
	return r
}

func CourseList(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "static/index.html")
}
