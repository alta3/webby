package app

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"text/template"

	"github.com/gorilla/mux"
	"gopkg.in/yaml.v2"
)

type PageData struct {
	Title        string
	Testimonials []Testimonial
}

type Testimonial struct {
	UUID               string  `yaml:"UUID"`
	Date               string  `yaml:"date"`
	Instructor         string  `yaml:"instructor"`
	StudentTestimonial string  `yaml:"student_testimonial"`
	TestimonialRating  float64 `yaml:"testimonial_rating"`
}

type Course struct {
	MarketingName   string
	Guaranteed      string
	Duration        string
	PublicDates     []string
	CourseDirectory string
	Price           string
	IsPrivate       bool
	Keywords        string
	CourseDir       string `json:"course_dir"`
	DurationDays    int    `json:"duration_days"`
	CourseCode      string `json:"course_code"`
	SelfPaced       bool   `json:"self_paced"`
	Objectives      string `json:"objectives"`
	Overview        string `json:"overview"`
	Specs           string `json:"specs"`
}

func readCoursesFromCSV(filePath string) ([]Course, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, fmt.Errorf("unable to open CSV file: %v", err)
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("unable to read CSV file: %v", err)
	}

	courseMap := make(map[string]*Course) // Use a map to hold courses by their code

	for _, row := range records[1:] { // Assuming the first row is headers
		if len(row) < 12 { // Ensure there are enough columns
			continue
		}
		courseCode := row[1] // Assuming courseCode is at index 1
		date := row[6]       // And date is at index 6

		// Check if the course already exists in the map
		if course, exists := courseMap[courseCode]; exists {
			if date == "private" {
				course.IsPrivate = true
			} else {
				course.PublicDates = append(course.PublicDates, date)
			}
		} else {
			newCourse := Course{
				Keywords:        row[0], // Adjust indices based on your CSV
				CourseCode:      courseCode,
				MarketingName:   row[2],
				Duration:        row[3],
				Guaranteed:      row[5],
				PublicDates:     []string{},
				CourseDirectory: row[11],
				Price:           row[4],
				IsPrivate:       date == "private",
			}
			if date != "private" {
				newCourse.PublicDates = append(newCourse.PublicDates, date)
			}
			courseMap[courseCode] = &newCourse
		}
	}

	var courses []Course
	for _, course := range courseMap {
		courses = append(courses, *course)
	}

	return courses, nil
}

// HomeHandler redirects to the courses page as per your original setup.
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	courses, err := readCoursesFromCSV("web/static/assets/webby_data.csv")
	if err != nil {
		log.Fatalf("Unable to read courses from CSV: %v", err)
	}

	// Fetch testimonials as before
	testimonials, err := FetchTestimonials()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	selectedTestimonials := SelectRandomTestimonials(testimonials, 10)

	// Pass courses and testimonials to the template
	data := struct {
		Title        string
		Testimonials []Testimonial
		Courses      []Course
	}{
		Title:        "Courses",
		Testimonials: selectedTestimonials,
		Courses:      courses,
	}

	renderTemplateWithData(w, "home.html", data)
}

// CoursesHandler for rendering the courses page.
func CoursesHandler(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "courses.html", "Courses")
}

func CoursePageHandler(w http.ResponseWriter, r *http.Request) {
	courses, err := readCoursesFromCSV("web/static/assets/webby_data.csv")
	if err != nil {
		log.Fatalf("Unable to read courses from CSV: %v", err)
	}

	vars := mux.Vars(r)
	courseCode := vars["coursecode"]
	var courseDetail *Course

	for _, course := range courses {
		if course.CourseCode == courseCode {
			courseDetail = &course
			break
		}
	}

	if courseDetail == nil {
		http.Error(w, "Course not found", http.StatusNotFound)
		return
	}

	// Fetch additional course details from JSON, if needed
	jsonURL := "https://static.alta3.com/webpage-data/courses_data.json"
	courseDetailFromJSON, err := FetchCourseDetailsFromJSON(jsonURL, courseCode)
	if err != nil {
		http.Error(w, "Unable to retrieve course details from JSON", http.StatusInternalServerError)
		return
	}

	// Merge JSON data into courseDetail
	if courseDetailFromJSON != nil {
		courseDetail.Overview = courseDetailFromJSON.Overview
		courseDetail.Objectives = courseDetailFromJSON.Objectives
		courseDetail.Specs = courseDetailFromJSON.Specs
		courseDetail.SelfPaced = courseDetailFromJSON.SelfPaced
		courseDetail.CourseDir = courseDetailFromJSON.CourseDir
	}

	// Pass the merged course details to the template
	data := struct {
		CourseDetail Course
	}{
		CourseDetail: *courseDetail,
	}
	renderTemplateWithData(w, "coursePage.html", data)
}

// Utility function to check if a slice contains a given string.
func contains(slice []string, str string) bool {
	for _, v := range slice {
		if v == str {
			return true
		}
	}
	return false
}

func FetchCourseDetailsFromJSON(jsonURL, courseCode string) (*Course, error) {
	resp, err := http.Get(jsonURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var courses []Course
	if err := json.NewDecoder(resp.Body).Decode(&courses); err != nil {
		return nil, err
	}

	for _, course := range courses {
		if course.CourseCode == courseCode {
			return &course, nil
		}
	}

	return nil, fmt.Errorf("course not found")
}

// EventsHandler for rendering the events page.
func EventsHandler(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "events.html", "Events")
}

func CoursePurchaseHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	courseCode := vars["coursecode"]
	date := vars["date"]

	// Placeholder for fetching course details - you'll need to implement this
	courseDetails, err := FetchCourseDetails(courseCode, date)
	if err != nil {
		http.Error(w, "Course not found", http.StatusNotFound)
		return
	}

	data := struct {
		Title         string
		CourseDetails Course // Assuming Course is a struct that holds course details
		Date          string
	}{
		Title:         "Purchase Course",
		CourseDetails: courseDetails,
		Date:          date,
	}

	renderTemplateWithData(w, "purchase.html", data)
}

// AboutUsHandler for rendering the about us page.
func AboutUsHandler(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "about.html", "About Us")
}

// BlogHandler for rendering the blog page.
func BlogHandler(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "blog.html", "Blog")
}

// ContactUsHandler for rendering the contact page.
func ContactUsHandler(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "contact.html", "Contact Us")
}

func SubmitNewsLetterHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		fmt.Fprintf(w, "yes we did it")
	} else {
		// If not a POST request, send a method not allowed status
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}

// renderTemplate is a helper function to parse and execute templates with the base template.
func renderTemplate(w http.ResponseWriter, templateName string, title string) {
	tmpl, err := template.ParseFiles("web/templates/base.html", "web/templates/"+templateName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data := PageData{
		Title: title,
	}

	// Changed to Execute, which automatically executes the first parsed template.
	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// renderTemplateWithData is a new function that will pass the data, including testimonials, to the template.
func renderTemplateWithData(w http.ResponseWriter, templateName string, data interface{}) {
	tmpl, err := template.ParseFiles("web/templates/base.html", "web/templates/"+templateName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// FetchTestimonials fetches and parses the testimonials YAML file.
func FetchTestimonials() ([]Testimonial, error) {
	resp, err := http.Get("https://static.alta3.com/webpage-data/testimonials/testimonials_good_short.yml")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error fetching testimonials: HTTP %d %s", resp.StatusCode, resp.Status)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var testimonials []Testimonial
	if err := yaml.Unmarshal(body, &testimonials); err != nil {
		return nil, err
	}

	return testimonials, nil
}

// SelectRandomTestimonials selects a random subset of testimonials.
func SelectRandomTestimonials(testimonials []Testimonial, count int) []Testimonial {
	rand.Shuffle(len(testimonials), func(i, j int) {
		testimonials[i], testimonials[j] = testimonials[j], testimonials[i]
	})

	if count > len(testimonials) {
		count = len(testimonials)
	}

	return testimonials[:count]
}

func FetchCourseDetails(courseCode, date string) (Course, error) {
	// Implement fetching logic here
	// For now, returning a placeholder
	return Course{
		CourseCode:      courseCode,
		MarketingName:   "Introduction to Cloud",
		Duration:        "5 Days",
		Guaranteed:      "Yes",
		CourseDirectory: "https://link-to-outline.pdf",
		Price:           "$2000",
	}, nil
}
