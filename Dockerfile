FROM golang:1.21.4 AS builder

# Assuming you add build steps here to compile your Go program...
# For example:
WORKDIR /webby
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o webserver .

# Now, start a new stage from scratch
FROM scratch

# Copy the CA certificates from the builder image
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# Corrected path for the binary. Assuming your binary is located at /webby/webserver in the builder stage
COPY --from=builder /webby/webserver /webserver

# Corrected path for the web directory
COPY --from=builder /webby/web /web

# Expose the port the server listens on
EXPOSE 8888

# Run the web server binary
CMD ["/webserver"]
