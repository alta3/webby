package main

import (
	"log"
	"net/http"

	"github.com/alta3/webby/pkg/app"
	"github.com/alta3/webby/pkg/static"
	"github.com/gorilla/mux"
)

func RegisterRoutes(router *mux.Router) {
	router.HandleFunc("/", app.HomeHandler).Methods("GET")
	router.HandleFunc("/courses", app.CoursesHandler).Methods("GET")
	router.HandleFunc("/events", app.EventsHandler).Methods("GET")
	router.HandleFunc("/about", app.AboutUsHandler).Methods("GET")
	router.HandleFunc("/blog", app.BlogHandler).Methods("GET")
	router.HandleFunc("/contact", app.ContactUsHandler).Methods("GET")
	router.HandleFunc("/home/submit-newsletter", app.SubmitNewsLetterHandler).Methods("POST")
	router.HandleFunc("/purchase/{coursecode:[a-zA-Z0-9-]+}/{date:[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}}", app.CoursePurchaseHandler).Methods("GET")
	router.HandleFunc("/course/{coursecode:[a-zA-Z0-9-]+}", app.CoursePageHandler).Methods("GET")

}

func main() {
	port := "8888" // Change this to your desired port

	r := mux.NewRouter()
	RegisterRoutes(r)

	static.ConfigureStaticFiles(r)

	log.Println("Server is starting on port", port+"...")
	err := http.ListenAndServe(":"+port, r)
	if err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
}
