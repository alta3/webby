This is a proof of concept site for Alta3 and a solid example for Kubernetes courses. 

Once downloaded, make sure you:

`go mod download`

Then you can:

`go run main.go` to simply run the program locally on port 8080.

You can alternately build the docker image:

`docker build -t webby:0.1 .` 

and once built run the image:

`docker run -p 8080:8080 webby:0.1`

And then visit localhost:8080.