module github.com/alta3/webby

go 1.21.4

require github.com/gorilla/mux v1.8.1

require gopkg.in/yaml.v2 v2.4.0
